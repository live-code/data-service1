export interface Device {
  id: number;
  name?: string;
  country?: string;
  os?: string;
  memory?: number;
  date?: number;
}
