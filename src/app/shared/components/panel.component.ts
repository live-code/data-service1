import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
    <div class="card" [ngClass]="'mb-' + paddingBottom">
      <div 
        class="card-header" 
        *ngIf="title"
        (click)="opened = !opened"
      >
        {{title}}
        
        <div class="pull-right">
          <i 
            [class]="icon"
            *ngIf="icon"
            (click)="iconHandler($event)"
          ></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class PanelComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() paddingBottom: 0 | 1 | 2 | 3 | 4 | 5 = 3;
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>()

  opened = true;

  iconHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit();
  }
}
