import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item" *ngFor="let item of items" (click)="tabClick.emit(item)">
        <a class="nav-link" [ngClass]="{'active': item.id === active?.id}">
          {{item[labelField]}}
        </a>
      </li>
    </ul>
  `,
  styles: []
})
export class TabbarComponent<T extends { id: number}> {
  @Input() items: T[];
  @Output() tabClick: EventEmitter<T> = new EventEmitter<T>();
  @Input() active: T;
  @Input() labelField = 'name';
}
