import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    
    <div style="border-radius: 10px; border: 1px dotted blue; padding: 10px; display: inline-block">
      <img [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&size=200,100&zoom=5&center=' + city" alt="">
    </div>
  `,
  styles: []
})
export class StaticMapComponent{
  @Input() city: string;

}
