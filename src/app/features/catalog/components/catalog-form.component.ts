import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-catalog-form',
  template: `
    <div class="alert alert-danger" *ngIf="error">
      c'è un errore
      <i class="fa fa-times" (click)="error = null"></i>
    </div>

    <form #f="ngForm" (submit)="save.emit(f)">

      <label *ngIf="nameInput.errors?.minlength">
        Campo troppo corto
        Mancano {{getMissingChars(nameInput)}} caratteri
      </label>

      <div
        class="bar"
        [style.width.%]="getPerc(nameInput)"
        [style.background-color]="getColor(nameInput)"
      ></div>

      <input
        class="form-control"
        [ngClass]="{'is-invalid': nameInput.invalid && f.dirty}"
        type="text"
        [ngModel]="active?.name"
        #nameInput="ngModel"
        placeholder="name"
        name="name"
        required
        minlength="5"
      >

      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">
            <i
              class="fa"
              [style.color]="cityInput.valid ? 'green' : 'red'"
              [ngClass]="{
                  'fa-asterisk': cityInput.errors?.required,
                  'fa-sort-numeric-asc': cityInput.errors?.minlength,
                  'fa-check': cityInput.valid
                }"
            ></i>
          </div>
        </div>

        <input
          class="form-control"
          [ngClass]="{'is-invalid': cityInput.invalid && f.dirty}"
          type="text"
          [ngModel]="active?.country"
          #cityInput="ngModel"
          placeholder="add a country"
          name="country"
          required
          minlength="3"
        >

      </div>

      <select [ngModel]="active?.os" name="os" class="form-control" required>
        <option [value]="null">Select the OS</option>
        <option value="android">Android</option>
        <option value="ios">Apple</option>
      </select>

      <div class="btn-group">

        <button
          class="btn"
          [ngClass]="{'btn-success': f.valid, 'btn-danger': f.invalid}"
          type="submit" [disabled]="f.invalid">
          {{active ? 'EDIT' : 'ADD'}}
        </button>

        <button type="button" (click)="clean.emit(f)">
          RESET
        </button>
      </div>
    </form>

  `,
  styles: []
})
export class CatalogFormComponent implements OnInit {

  @Input() error: boolean;
  @Input() active: Device;
  @Output() clean: EventEmitter<NgForm> = new EventEmitter<NgForm>();
  @Output() save: EventEmitter<NgForm> = new EventEmitter<NgForm>();

  constructor() { }

  ngOnInit(): void {
  }



  getMissingChars(input: NgModel) {
    return input.errors?.minlength?.requiredLength - input.errors?.minlength?.actualLength
  }

  getPerc(input: NgModel) {
    if (!input.errors?.minlength) {return 0};

    const value = (input.errors?.minlength?.actualLength / input.errors?.minlength?.requiredLength) * 100;
    return value;
  }

  getColor(input: NgModel) {
    const value = (input.errors?.minlength?.actualLength / input.errors?.minlength?.requiredLength) * 100;

    if (value < 50) {
      return 'green';
    } else {
      return 'orange'
    }

  }

}
