import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-catalog-list',
  template: `
    <div class="container">
      
      <hr>

      <li
        class="list-group-item"
        *ngFor="let device of devices"
        [ngClass]="{'active': device.id === active?.id}"
        (click)="setActive.emit(device)"
      >
        <i
          class="fa"
          [style.color]="device.os === 'android' ? 'lightgreen' : 'grey'"
          [ngClass]="{
            'fa-android': device.os === 'android',
            'fa-apple': device.os === 'ios'
          }"
        ></i>
        {{device.name}} ({{device.id}})

        <div class="pull-right">
          <i class="fa fa-trash" (click)="deleteHandler(device, $event)"></i>
        </div>
      </li>
    </div>
  `,
  styles: []
})
export class CatalogListComponent {
  @Input() devices: Device[]
  @Input() active: Device;
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>()
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>()

  deleteHandler(device: Device, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(device);
  }
}
