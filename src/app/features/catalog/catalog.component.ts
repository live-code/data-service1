import { Component } from '@angular/core';
import { Device } from '../../model/device';
import { NgForm, NgModel } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-catalog',
  template: `

    <app-catalog-form
      [active]="active"
      [error]="error"
      (save)="save($event)"
      (clean)="resetHandler($event)"
    ></app-catalog-form>

    {{devices?.length}} Devices

    <app-catalog-list
      [active]="active"
      [devices]="devices"
      (setActive)="setActive($event)"
      (delete)="deleteHandler($event)"
    ></app-catalog-list>

  `,
  styles: [`
    .android { background-color: lightgreen }
    
    .bar { 
      background-color: red;
      width: 100%;
      height: 10px;
      transition: all 1s cubic-bezier(0.86, 0, 0.07, 1);
    }
  `]
})
export class CatalogComponent {
  devices: Partial<Device>[];
  active: Partial<Device>;
  error: boolean;

  constructor(private http: HttpClient) {
    this.getAll();
  }

  getAll() {
    this.http.get<Partial<Device>[]>('http://localhost:3000/devices')
      .subscribe(res => this.devices = res);
  }

  save(form: NgForm) {
    if (this.active) {
      this.edit(form.value);
    } else {
      this.add(form.value);
    }
  }

  edit(formData: Partial<Device>) {
    this.http.patch(`http://localhost:3000/devices/${this.active.id}`, formData )
      .subscribe(res => {
        const index = this.devices.findIndex(d => d.id === this.active.id)
        this.devices[index] = res;
      });
  }

  add(formData: Partial<Device>) {
    this.http.post<Partial<Device>>('http://localhost:3000/devices', formData)
      .subscribe(res => {
        this.devices.push(res);
        this.active = this.devices[this.devices.length - 1 ];
      });


  }

  deleteHandler(device: Partial<Device>) {

    this.http.delete(`http://localhost:3000/devices/${device.id}` )
      .subscribe(
        () => {
          const index = this.devices.findIndex(d => d.id === device.id);
          this.devices.splice(index, 1);
        },
        err => {
          this.error = true;
          setTimeout(() => {
            this.error = false
          }, 1000)
        }
      );

  }

  setActive(device: Partial<Device>) {
    this.active = device;
  }

  resetHandler(f: NgForm) {
    f.reset();
    this.active = null;
  }
}
