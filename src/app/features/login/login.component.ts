import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { count } from 'rxjs/operators';

interface Country {
  id: number;
  name: string;
  desc: string;
  children: City[];
}
interface City {
  id: number;
  cityName: string;
}
@Component({
  selector: 'app-login',
  template: `

    <app-tabbar
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="countryClickHandler($event)"
    ></app-tabbar>

    <app-tabbar
      [items]="activeCountry?.children"
      labelField="cityName"
      [active]="activeCity"
      (tabClick)="cityClickHandler($event)"
    ></app-tabbar>

    <app-panel
      [title]="'DESCRIZIONE'"
      icon="fa fa-external-link-square"
      (iconClick)="activeCountry = countries[2]"
    >
      {{activeCountry?.desc}}
    </app-panel>

    <app-static-map [city]="activeCity?.cityName"></app-static-map>

  `,
})
export class LoginComponent {
  countries: Country[];
  activeCountry: Country;

  cities: City[];
  activeCity: City;

  constructor(http: HttpClient) {
    http.get<Country[]>('http://localhost:3000/countries')
      .subscribe(res => {
        this.countries = res;
        this.activeCountry = res[0];
        this.activeCity = this.activeCountry.children[0];
      });
  }

  countryClickHandler(country: Country) {
   this.activeCountry = country;
   this.activeCity = country.children[0];
  }

  cityClickHandler(city: City) {
    this.activeCity = city;
  }
}
