import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="login" routerLinkActive="bg-warning">login</button>
    <button routerLink="catalog" routerLinkActive="bg-warning">catalog</button>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {}
