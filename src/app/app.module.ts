import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CatalogComponent } from './features/catalog/catalog.component';
import { LoginComponent } from './features/login/login.component';
import { RouterModule } from '@angular/router';
import { TabbarComponent } from './shared/components/tabbar.component';
import { StaticMapComponent } from './shared/components/static-map.component';
import { PanelComponent } from './shared/components/panel.component';
import { CatalogListComponent } from './features/catalog/components/catalog-list.component';
import { CatalogFormComponent } from './features/catalog/components/catalog-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    LoginComponent,

    // shared
    PanelComponent,
    TabbarComponent,
    StaticMapComponent,
    CatalogListComponent,
    CatalogFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'catalog', component: CatalogComponent },
      { path: '', component: LoginComponent},
      { path: '**', redirectTo: ''},
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
